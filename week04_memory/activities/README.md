# Activity 1 - Dynamic and Statically Linked Libraries

Modify "welcome.h", "welcome.c", and "program.c" so they have both a global variable within the library and also one in the main program.  Create both static and dynamic versions of this library. Use "objdump" to gain an understanding of where these variables are located.  
Answer the following:
+ What is the difference between the static/dynamic approaches? 
+ Does PIC option change what happens?  
+ With the dynamic library have 2 processes running the program at the same time.  Do they use the same physical memory for library?   Using /proc/pid/maps to see if they use the same virtual addresses.  Use the pape_map.c program to work out the physical addresses for these pages,  are the physical addresses the same?

# Activity 2 - Memory - mmap

The "mycpy.c" program is a simple programming for copying a file using the "read" and "write" system calls.  Compile and run this program.   Modify this program so it uses "mmap" to copy a file.   This involves the following:
+ copy "mycpy.c" to a new file called "mycpy_mmap.c" (you can use the "mycpy" program to do this copy!)
+ remove the code that uses read/write for the copying
+ truncate the destination file to the required size (use ftruncate) - as the name suggests truncate will make files smaller,  but it can also be used to make them bigger.
+ mmap the source file into a region of private memory that you can read
+ mmap the destination file into a region of shared memory that you can read and write 
+ close the files
+ copy the memory from source to destination (you can use memcpy or just a for loop and assignment)
+ unmap the files (munmap)

Why private/shared memory for source/destination?  Is this required?  Do you think mmap would be more or less efficient than the read/write approach?


# Activity 3 - Physical and Virtual Memory - Accessing a running processes data

The aim of this activity is to modify a part of the memory of a running process from completely outside that process. In a separate terminal run the dynamic library you created in Activity 1.  So it should repeatedly say "Hello!!".   The objective is to, from outside of this process, modify its memory such that it repeatedly says "Goodbye".

The basic steps involved in doing this are:
+ work out the process id using ps or top
+ Use /proc/pid/maps to work out the virtual address in which the "Hello!!" will be located.
+ "peek_process_mem.c" uses ptrace to peek memory within another process.  Read over this code, then use it to find the location of "Hello!!".
+ Copy "peek_process_mem.c" to "poke_process_mem.c" and modify the code so you can write "Goodbye" over the top "Hello!!".  All going well you should see the "Hello" change in the running process.  

