
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>


int main(int argc, char ** argv){
    if(argc!=2){
      printf("Usage: read_pfn PFN\n");
      return -1;
   }
   char * frame;
   unsigned long pfn = strtol(argv[1], NULL, 16);
   unsigned long physical_address = pfn * 4096L;
   printf("Reading /dev/mem at address : %lX\n", physical_address);
   int fd = open("/dev/mem",O_RDONLY);
   if (fd == -1) {
      printf("Problem with opne\n");
      exit(-1);
   }
   frame = mmap(NULL,4096,PROT_READ, MAP_SHARED, fd, physical_address);
   if (frame == -1) {
      printf("Problem with mmap\n");
      exit(-1);
   }
   for (int i = 0;i<4096;i++) {
        write(1, &(frame[i]), 1);
   }
   
}
