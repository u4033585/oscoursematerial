
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ptrace.h>
#include <sys/wait.h>



// peek_process_mem - have a look at the memory of another process 
// Eric McCreath 2022

int main(int argc, char ** argv){
    if(argc!=4){
        printf("Usage: peek_process_mem pid virtual_address count\n");
        return -1;
    }
    int pid = atoi(argv[1]);
    unsigned long virtual_address = strtol(argv[2], NULL, 16);
    int count = atoi(argv[3]);
   
    ptrace(PTRACE_ATTACH, pid, NULL, NULL);
    waitpid(pid, NULL, 0);
    for (int i = 0;i<count;i++) {
        long res = ptrace(PTRACE_PEEKDATA, pid, virtual_address + i, NULL);
        printf("%d: %X %c\n", i, (unsigned char) (res & 0xFF), (char) (res & 0xFF) );
    }
    ptrace(PTRACE_DETACH, pid, NULL, NULL);
}
