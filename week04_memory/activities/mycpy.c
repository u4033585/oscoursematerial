#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>
#include <stdint.h>

#define BUFSIZE 512

// mycpy - copy files using read/write - Eric McCreath 2010,2016,2022
    
void problem(char *str) {
   fprintf(stderr, "%s\n",str);
   exit(1);
}

uint8_t buffer[BUFSIZE];

int main(int argc, char *argv[]) {
   int fs, fd;
   struct stat fsstat;

   if (argc != 3) 
      problem("Usage : mycpy <source> <destinatio>");

// open the files
   fs = open(argv[1],O_RDONLY);
   if (fs == -1) problem("problem opening source file");
   fstat(fs,&fsstat);
   fd = open(argv[2],O_RDWR|O_CREAT|O_TRUNC,fsstat.st_mode);
   if (fd == -1) problem("problem opening destination file");


// copy the data - does not deal with all error cases
   int copied = 0;
   int read_res, write_res;
   while (copied < fsstat.st_size) {
       read_res = read(fs,buffer,BUFSIZE);
       if (read_res == -1) problem("problem reading file"); 
       int written = 0;
       while (written < read_res) {
            write_res = write(fd, &buffer[written], read_res-written);
            if (write_res == -1) problem("problem writing file"); 
            written += write_res;
       }
       copied += read_res;
   }

// close the files
   close(fs);
   close(fd);


   return 0;
}
