#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>
#include <stdint.h>

// hello_mmap - using mmap to write "hello" to a file - Eric McCreath 2022
    
void problem(char *str) {
   fprintf(stderr, "%s\n",str);
   exit(1);
}


int main(int argc, char *argv[]) {
   int fd;
   uint8_t *md;
   
// open the file
   fd = open("Hello.txt",O_RDWR|O_CREAT|O_TRUNC,00666);
   if (fd == -1) problem("problem opening destination file");

// truncate the destination file to the length of "hello"
   ftruncate(fd, 5);


   md = mmap(NULL,5 ,PROT_READ|PROT_WRITE, MAP_SHARED, fd,0);
   if (md == NULL) problem("problem mmaping destination");
 
// close the file
   close(fd);
   
// copy 
   memcpy(md, "hello", 5);
   
   printf("Press Any Key to Continue\n");  
   getchar();    

// unmap the file
   munmap(md,5);

   return 0;
}

