
# Instructions for creating libraries

So we have a simple program "program.c" that wishes to make use of the "welcome" library.  

Now if we wished to we could compile this without using static/dynamic libraries using:
```
gcc -o program program.c welcome.c
```
Or if we wished to do it in stages creating the objects and then linking:
```
gcc -c -o program.o program.c
gcc -c -o welcome.o welcome.c
gcc -o program program.o welcome.o
```
However if "welcome" became useful for different programs we could turn it into a library.  

## Statically Linked Library

We first make the object:
```
gcc -c -o welcome_static.o welcome.c
```
We can see the symbols used with:
```
nm welcome_static.o
```

We can then make the static library by just creating an achieve of the object files:
```
ar -cvq libwelcome.a welcome_static.o
```

We can list the object files in the library:
```
ar -t libwelcome.a
```

Now we have a library we can use to link with our program using:
```
gcc -o program program.c libwelcome.a
```
or
```
gcc -o program program.c -L<path> -lwelcome
```

To have a look at what is happening within the program you can use:
```
objdump -d program
objdump -d welcome_static.o
objdump -t welcome_static.o
objdump -r welcome_static.o
strace ./program
ltrace ./program
ldd program
```

## Dynamically Linked Library

For dynamically linked libraries we need to re-compile it so it is PIC (Position Independent Code).  

We first make the object:
```
gcc -c -fPIC -o welcome.o welcome.c
```
We now create a shared library that is linked but not embedded into the executable.  So in unix they are called ".so", in windows they are ".dll" files.
```
gcc -shared -Wl,-soname,libwelcome.so.1 -o libwelcome.so.1.0 welcome.o
```
Make a link for gcc to find it:
```
ln -sf libwelcome.so.1.0 libwelcome.so
```

Make a link for loader to find it:
```
ln -sf libwelcome.so.1.0 libwelcome.so.1
```

Compile main program and link with shared object
```
gcc -no-pie -L. program.c -lwelcome -o program
export LD_LIBRARY_PATH=/home/ericm/oscoursematerial/week03_memory/examples:$LD_LIBRARY_PATH
```
## Exploring the virtual memory map

So we can work out the pid of the "program" as it executes using:
```
ps -aux
```
Then cd into the proc directory for that process:
```
cd /proc/<pid>
```
And have a look at the maps:
```
sudo cat maps
```

The "pagemap" file in proc for the process will tell you the physical addresses for the virtual addresses of the running process.   "pagemap" is not text so you need a program to understand it (it is also not that complex as it is just a big array of pagemap entries indexed by the page number).  "page_map.c" is a simple program looks up this table.   Noting you need to run "page_map" as sudo to gain the needed permission to access the "pagemap" proc entry. 



