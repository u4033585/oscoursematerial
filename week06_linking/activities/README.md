# Linking

In this activity we are going to compile up a simple program,  in a similar way we did in week 3.  However, in this case we are going to take a closer look at the ELF and we are also going to step through the program with a debugger. 

Have a look at the "welcome.c" and "program.c".   When is the variable "talk_count" referenced?  What would you expect the program to do when you run it?

Compile and run it with:
```
gcc -g -c -o welcome.o welcome.c
gcc -g -o program program.c welcome.o
./program
```

The "-g" options enables us to attach a debugger and step through the execution of the program.

To run the program with the debugger, execute:
```
gdb program
```
Once in the debugger you can start the program running with:
```
(gdb) start 
```

Try the following commands:
+ s
+ bt
+ si
+ disass
What do they do?

You can use "control-d" to exit. 

Make a note to the virtual address location of where "talk" is called from and also the address location where "talk" is implemented. 

Use:
```
objdump -d program 
```

to disassemble  the code and see how this call takes place. 

In a similar way look at the global variable "talk_count" and work out how it is referenced.  

Noting the "mov" instruction moves data between memory and registers.  e.g. "mov 0x10(%rip),%eax" moves the data at memory location of the instruction pointer plus offset 0x10 into the eax register.  Whereas "mov (%rax),%eax" moves the data from the address location that is in register %rax into register %eax.

Now repeat the same evaluation although this time using a dynamically linked approach.  So the commands to compile it up with are:
```
gcc -g -c -fPIC -o welcome_dyn.o welcome.c
gcc -g -shared -Wl,-soname,libwelcome.so.1 -o libwelcome.so.1.0 welcome_dyn.o
ln -sf libwelcome.so.1.0 libwelcome.so
ln -sf libwelcome.so.1.0 libwelcome.so.1
gcc -L. program.c -lwelcome -o program_dyn
export LD_LIBRARY_PATH=/home/ericm/oscoursematerial/week06_linking/activities:$LD_LIBRARY_PATH

```
(you will need to correct the LD_LIBRARY_PATH in the above)

In particular have a look at the difference between how the "talk_count" is referenced within the main method compared to how it is referenced within the talk function.   What is happening here?


Have a look at the sections in the elf:
```
readelf -S program
```

Also the relocations:
```
readelf -r program
```

Can you find "talk" and "talk_count" ?








