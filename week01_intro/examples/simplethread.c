
#define _GNU_SOURCE

#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>
#include<stdlib.h>
#include <sys/types.h>

#include <unistd.h>


/*
 Eric McCreath 2022
*/ 
/* gcc -o simplethread simplethreads.c -lpthread
 */

pthread_t thread;

void show(char *str) {
    printf("%s  pid:%d tid:%d\n",str, getpid(), gettid());
}

void routine() {
    show("Routine ");
}

int main(int argc, char *argv[]) {
   
    show("Start   ");
     
    pthread_create(&thread,NULL,(void *) routine,NULL);
    
    show("Created ");
    
    pthread_join(thread, NULL);
    
    show("End     ");
}
