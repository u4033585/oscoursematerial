#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

// Eric McCreath 2022

int main() {
    char command[100];
    pid_t p;
    int status;
    char *args[] = {NULL, NULL}; 
    printf("> ");
    while((scanf("%s",command) == 1) && !(strcmp(command,"bye") == 0)) {
        args[0] = command;
        p = fork();
        if (p == 0) {
           execvp(command,args);
           exit(EXIT_FAILURE);
        }
        wait(&status);
        printf("> ");
     }
     return 0;
}
