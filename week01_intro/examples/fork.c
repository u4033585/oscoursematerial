#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

// Eric McCreath 2022

int main() {
   int p;
   printf("Start\n");
   p = fork();
   printf("End pid:%d\n", p);
   return 0;
}
