#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>
#include<stdlib.h>

/* Based on code from Arpaci-Dusseau, Operating Systems Three Easy Pieces p9
 Eric McCreath 2018
*/ 
/* gcc -o threads threads.c -lpthread
 */

pthread_t threadA, threadB;

int loop;

//volatile 
int count = 0;

void routine() {
  int j;
  for (j=0;j<loop;j++) {
     count++;
  }
}

int main(int argc, char *argv[]) {
   
    if (argc != 2) {
       fprintf(stderr,"usage: threads number\n");
       exit(1); 
    }
   
    loop = atoi(argv[1]);
    pthread_create(&threadA,NULL,(void *) routine,NULL);
    pthread_create(&threadB,NULL,(void *) routine,NULL);
   
    
    pthread_join(threadA, NULL);
    pthread_join(threadB, NULL);
    
    printf("%d\n", count);
}
