#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

// Eric McCreath 2022

int main() {
     char *args[] = {"/bin/ls", NULL};
     printf("Start\n");
     execvp("/bin/ls",args);
     printf("End\n");
     return 0;
}
