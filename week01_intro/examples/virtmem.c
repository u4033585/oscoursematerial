#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

// virtmem.c - example code for show the idea of virtual memory Eric McCreath 2018
// based on mem.c on p7 of Operating Systems Three Easy Pieces Arpaci-Dusseau


//  gcc -o virtmem virtmem.c
// cat /proc/sys/kernel/randomize_va_space
//  echo 0 | sudo tee /proc/sys/kernel/randomize_va_space 
// ./virtmem A & ./virtmem B &


void spin() {
  int count = 0;
  int i,j;
  for (i=0;i<50000;i++) {
    for (j=0;j<10000;j++) {
      count += i;
    }
  }
}


int
main(int argc, char *argv[])
{
   char *name;
   if (argc != 2) {
      fprintf(stderr, "usage: virtmem <name>\n");
      exit(1);
   }
   name = argv[1];

   int *intmem;
   intmem = (int *) malloc(sizeof(int));

  *intmem = 0;
   printf("intmem address: %08lx\n", (unsigned long) intmem);
   printf ("name address: %08lx\n", (unsigned long) &name);
   printf ("spin address: %08lx\n", (unsigned long) spin);
   int j = 0;
   while (j<20) {
      spin();
      *intmem = *intmem + 1;
      printf("%s : %d\n", name, *intmem); 
      j++;
   }
   return 0;
} 
