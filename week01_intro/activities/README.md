# Activity 1


## Step 1 - download Ubuntu iso

Download "ubuntu-22.04.1-desktop-amd64.iso",  a good place to get it is from the Aarnet mirror.  [https://mirror.aarnet.edu.au/pub/ubuntu/releases/22.04.1/](https://mirror.aarnet.edu.au/pub/ubuntu/releases/22.04.1/)

Once downloaded check the sha256sum using "sha256sum",  it should be:
```
c396e956a9f52c418397867d1ea5c0cf1a99a49dcf648b086d2fb762330cc88d *ubuntu-22.04.1-desktop-amd64.iso
```

What are the chances that a random file would have this same sha256sum?

What are some potential weaknesses in this approach for authenticating the iso?

## Step 2 - copy iso to USB (optional) 

The first thing you need to do is work out the device node that the USB drive enumerates to when plugged in.   A simple way of doing this is using the commands "lsblk", "df", or "mount".  Run the command before and after inserting the usb.  You should be able to work out the device node,  it should be of the form "/dev/sdX" where "X" is "a", "b", "c", .... .   WARNING TAKE GREAT CARE WITH THIS - GET IT WRONG AND YOU COULD WIPE YOUR MAIN HARDDISK! 

In my case it was "/dev/sde" so "lsblk" had:
```
sde      8:64   1  14.5G  0 disk 
├─sde1   8:65   1   3.6G  0 part /media/ericm/Ubuntu 22.04.1 LTS amd64
├─sde2   8:66   1   4.2M  0 part 
└─sde3   8:67   1   300K  0 part 
```
"df" had:
```
/dev/sde1         3732528   3732528          0 100% /media/ericm/Ubuntu 22.04.1 LTS amd64
```
If any file systems on the usb are mounted you need to unmount them before copying the iso to the device.  This can be done using the "umount" command. So in my case:

```
sudo umount /dev/sde1
``` 

We write the iso to the entire disk this involves copying the entire file to the node for the block device.  One approach for doing this is using the command "dd".  The man page for "dd" states it "convert and copy a file".  Have a skim over the description, can you add a bit more detail on what "dd" does. 

So a command for copying the iso to the USB drive is: :
  
```
sudo dd if=ubuntu-22.04.1-desktop-amd64.iso of=/dev/sdX bs=1M status=progress conv=fsync
```
Remember to replace X with the letter you found at the start of this step.

Which options in the above could be removed and it would still work?


## Step 3 - install and run virtual box

In Ubuntu to install virtualbox you need root access and from a terminal run:

```
sudo apt-get update
sudo apt-get install virtualbox
```

then to run virtualbox you just use:

```
virtualbox
```

## Step 4 - set up a virtual machine 

For the most part it is just a matter of clicking "new" and follow the options.  A few things to note: 
+ use a dynamic vdi image,  setting its size to ~50G noting that it will only use what is need starting at ~9G.   
+ it is good to increase the base memory and number of processors I often find putting it about in the middle of the "green" zone is a good starting point.  

## Step 5 - install Ubuntu

To install Ubuntu,  under "Storage" you add the iso file to the IDE controller.   This is just like inserting the DVD into the machine.  Then "Start" running the machine.

Once started you install Ubuntu.  For the most part just follow the default options.  I just used a "minimal" desktop image.  Make certain you remember the password you set for your account. 

## Step 6 - install guest additions (optional)

Once logged into your new virtual machine "Insert guest additions CD image..." (under "Devices" menu).

Once inserted you should be able to access it so run:
```
cd /media/your_user_name/VBox_GAs_6.1.38
./autorun.sh
```

Reboot the virtual machine. 


## Step 7 - installing and cloning the course repo

Install git:
```
sudo apt install git
```

Then clone the repo:
```
git clone https://gitlab.anu.edu.au/u4033585/oscoursematerial.git
```

Also it is good to do an update of Ubuntu (this will take a bit of time - so do it in a different terminal in the background):
```
sudo apt-get update
sudo apt-get upgrade
```

## Step 8 - installing and running a few commands

Some of the commands will need installing this can be done using "sudo apt-get install package_name" as needed.

Try the following commands:
```
ps -auxwww
```
and:
```
pstree
```
also the "System Monitor" will give you the list of processes running.

How many processes are running?

What is the process using the most memory?

What is the process using the most CPU?

Using the "ps -auxwww" command determine the different states the processes are in?  What is the distribution of these states?  Which processes or process is currently running? Why is this the case?  (hint man ps and have a look a the "PROCESS STATE CODES")

Compile the "helloworld" program in this weeks activities directory.
```
gcc -o helloworld helloworld.c
```

Try using "strace" and "ltrace" on the "helloworld" program.

So:
```
strace ./helloworld
```
and
```
ltrace ./helloworld
```
What is the difference between what these output?


Compare this with a python version of the same program.

So:
```
strace python3 helloworld.py
```
and 
```
ltrace python3 helloworld.py
```

# Activity 2 - processes


## Step 1 - using strace/ltrace to understand what a program does

Have a read of the code in 'obfuscated.c'.  Can you tell what it does?  Any guesses?

Compile and run it.  What does it do?

Use 'strace'/'ltrace' to see if it does anything else.

What other approaches could you use to understand what "obfuscated.c" does? 

## Step 2 - fork and following threads with strace

Have a read of 'simplefork.c'.  Before you run it can you understand what it does?  Can you predict what it would print?

Compile and run it.  Did you get it right? Would the ordering of the printing always be the same?

Now use 'strace' to see the system calls used.

Try the "-f" option and then try the "-ff -o simplefork" options.  Lookup the man page to see what they do.  


## Step 3 - challenge

Compile and run 'secret01.c', but don't look at the code until you have completed this step.

Using what the program outputs along with strace (hint - using "-ff -o sec01" will make it a bit easier) have a guess of what the code in 'secret01.c' would be.

Once you have done this have a look at 'secret01.c'.  Did you guess the same?


# Activity 3 - threads


Compile and run 'thread_shared_data.c'.  Noting you need to compile with the pthread library:

```
gcc -o thread_shared_data thread_shared_data.c -lpthread
```

So this program creates 10 threads each of the threads adds 1 to a shared global integer called "data".   "data" starts off at 0 so at the end of the program you would expect it to hold 10.  Right?? 

What does it do when run?  Run it a few times.

Modify SPIN_DELAY to 0.  Re-compile and run.  What is the output now? Why is it different?    Try also a larger SPIN_DELAY. 

 





















