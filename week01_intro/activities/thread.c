#include <stdio.h>
#include <unistd.h>
#include<pthread.h>
#include<semaphore.h>
/* gcc -lpthread -o thread thread.c */

void work() {
  int count = 0;
  int i,j;
  for (i=0;i<30000;i++) {
    for (j=0;j<100000;j++) {
      count += i;
    }
  }
}

void routine() {
     printf("child: hello\n");
     work();
     printf("child: pid %d\n", getpid());
}

main () {
     pthread_t t1, t2;
     pthread_create(&t1,NULL,(void *) routine, NULL);
     pthread_create(&t2,NULL,(void *) routine, NULL);
     
     printf("parent: pid %d\n", getpid());
     pthread_join(t1,NULL);
     pthread_join(t2,NULL);
  
}

