#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>

/* gcc -o thread_shared_data thread_shared_data.c -lpthread*/

pthread_t t[10];
int data; 

#define SPIN_DELAY 100

int spin() {
  int i,j;
  int x = 0;
  for(i=0;i<SPIN_DELAY;i++) for(j=0;j<SPIN_DELAY;j++) x++;
  return x;
}

void routine(int i) {
  int loc = data;
  int sum = 0;
  printf("Running : %d, %d\n", i, data);
  sum = spin();
  data = loc+1;
  printf("Done : %d, %d, %d\n", i, data,sum);
}

int
main() {
  int i;
  data = 0;
  printf("Start : %d\n",data);
  for (i=0;i<10;i++) pthread_create(&t[i],NULL,(void *) routine,(void *) (long) i);
  printf("Created10: %d\n",data);

  
  for (i=0;i<10;i++) pthread_join(t[i], NULL);
  printf("End : %d\n", data);

  return 0;
}
