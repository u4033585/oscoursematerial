#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

// Eric McCreath 2022

int main () {
  int res;

  for (int i = 0; i<3; i++) {
      res = fork();
      printf("i: %d res: %d, mypid: %d\n", i, res, getpid());
  }
  return 0;
}

