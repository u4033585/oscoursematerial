#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>


// Eric McCreath 2012, 2014


int main () {
  int res;
  int status;

 
  res = fork();
  if (res == 0) {
      printf("child: hello from the child\n");
      printf("child: res was %d my pid %d\n",res, getpid());
      exit(0);
  } else {
      printf("parent: hello from the parent\n");
      printf("parent: res was %d my pid %d\n",res, getpid());
      wait(&status);
      printf("parent: status : %d\n", status);
  }
  return 0;
}

