# Activity 1 - The Virtual File System

Have a read of "The Virtual File System" section in (Design and Implementation of the Second Extended Filesystem)[http://e2fsprogs.sourceforge.net/ext2intro.html].

The most of the "set of functions" that need to be implemented for a new filesystem are defined in the following structures:
```
struct file_system_type
struct super_operations
struct inode_operations 
struct file_operations
```
these are given in "include/linux/fs.h".  Have a look at these structures.   In particular what is the difference between the functions defined in "inode_operations" and "file_operations"?


Note that directories are just a special type of file.  So the approach used in the virtual file system is to associate a different set of "inode_operations" and "file_operations" with directories compared to regular files.  In "inode_operations" and "file_operations" what operations relate to directories vs regular files?


# Activity 1 - Reverse Engineering the VVSFS

A simple file system has been implemented, called the vvsfs.  The aim of this first activity is to gain an understanding of this filesystem and it's limitations
without looking at the code (reverse engineering the filesystem).  To do this you can install, create and mount an instance of the filesystem,  and also looking at the raw blocks as you add files.  So make changes and see how these changes effect the raw blocks,  from this you should be able to quickly gain an understanding of the vvsfs. 

So to get you started complete the following:
+ Use "make" to create the module and the various helper utilities.
+ Load the module into the kernel with insmod (and sudo).
+ Check the filesystem has been registered (cat /proc/filesystems).
+ make an empty directory to mount the vvsfs, such as: "mkdir testdir". 
+ make an empty file that is exactly 51200 bytes long to use as the raw block device: "dd of=vvsfs.raw if=/dev/zero bs=512 count=100"  (this tells you a bit about the vvsfs)
+ make an initial vvsfs filesystem instance on this raw file: "./mkfs.vvsfs vvsfs.raw"
+ mount the vvsfs: "sudo mount -t vvsfs -o loop vvsfs.raw testdir"
+ use "cat /proc/mounts" to check that it was mounted okay.
+ 'cd' into the directory and try a few different file operations.
+ create two files, say "file1" and "file2", write some data into them. e.g. "echo Hello > file1.txt"
+ unmount the file system with "umount" 
+ have a look at the raw data in "vvsfs.raw"

So to understand the filesystem better you will need to remount it create some more files and see how that is reflected in the raw blocks (xxd is a handy tool for viewing raw data).

Answer the following questions:
+ What is the largest file that this filesystem can store?
+ What is the largest number of files that can be stored?
+ What operations are not implemented for the filesystem?
+ Is information about users/groups and permissions stored?
+ What is the largest file name that can be stored?
+ How do directories work?
+ How is free space managed?
+ vvsfs is an acronym.  Guess what do the letters stand for?

Once this is done have a look through the code in vvsfs.c.

# Activity 2 - Fixing Directory Size Bug

As the number of entries in the mounted directory increases, the size of
this file (directories are just special files in UNIX) should also increase.
However, a mounted vvsfs shows that the number of bytes taken up by the directory is zero???
This is a bug. It is basically due to the fact that the vfs caches a copy of the inode
information and the vvsfs also maintains a copy of the inode for this directory.
When a new file is added to the directory the vvsfs inode's size is updated, but the 
size in the vfs inode is not updated, hence, when you do a "ls -als testdir" the size of the
vvsfs directory is zero.


Fix this bug. (Hint, update the directories inode's "i_size" when it changes in __vvsfs_create__ or read in the directory in __vvsfs_readdir__ .  When you change the inode's "i_size" you should also mark the inode as being "dirty" using __mark_inode_dirty(struct inode *inode)__ )

Check that this modification worked.


# Activity 3 -  Adding the ability to "rm" a file (optional)

Currently you are unable to delete files within this filesystem.   In this activity your aim is to fix this limitation. 

To be able to delete files you need to add an extra routine called "vvsfs_unlink".  And then 
add the pointer to this routine in the structure
"vvsfs_dir_inode_operations".

What are the parameters of this routine? (look in /usr/include/linux/fs.h or
have a look at how another file system (e.g. minix, ext2) does this. Hint - you will need to mark_inode_dirty and inode_dec_link_count for the virtual files systems inode of the file your are deleting).   What will you need to change in the data stored on the within the filesystem?  What blocks will you need to read/modify/write to complete this change?

The unlink routine is most like the "vvsfs_lookup", use this routine
as a starting point for your code.

## References

* (Design and Implementation of the Second Extended Filesystem)[http://e2fsprogs.sourceforge.net/ext2intro.html] Card, Ts'o, and Tweedie

