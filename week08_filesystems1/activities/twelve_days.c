#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

// to compile and run use
// gcc -o twelve_days twelve_days.c


void twelve_days_of_christmas() {
    printf("On the first day of Christmas my true love sent to me\n");
    printf("A partridge in a pear tree\n");
    printf("\n");
    printf("On the second day of Christmas my true love sent to me\n");
    printf("Two turtle doves,\n");
    printf("And a partridge in a pear tree.\n");
    printf("\n");
    printf("On the third day of Christmas my true love sent to me\n");
    printf("Three French hens,\n");
    printf("Two turtle doves,\n");
    printf("And a partridge in a pear tree.\n");
}


int main() {

    int fd = open("twelve_days.txt", O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    
    // add dup calls here to redirect stdout to the file and save the standard out file descriptor

    twelve_days_of_christmas();
   
    close(fd); 
   
    // add a dup call here to restore standard out so the "Bye Bye..." is printed to the terminal. 
   
    printf("Bye Bye...\n");

    return 0;
}
