# Activity 1 - Making and Mounting a filesystem

In this first activity you will create a new filesystem then mount it within your root directory,  add some files,  then duplicate this filesystem by copying the raw blocks.  Some of these operations can take some time so we will just create a small partition within the block device (in this case USB).

Plug in your USB disk and using the "Disks" GUI application to create two 64MB FAT partition within the drive (you may need to delete the existing partition to make space for this.).

Once this is done you should be able list the block devices and see the partitions you have made.

```
ericm@ericm-VirtualBox:~$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
 :        :     :     :       :
sda      8:0    0  50.7G  0 disk 
├─sda1   8:1    0     1M  0 part 
├─sda2   8:2    0   513M  0 part /boot/efi
└─sda3   8:3    0  50.2G  0 part /var/snap/firefox/common/host-hunspell
                                 /
sdb      8:16   1   3.8G  0 disk 
├─sdb1   8:17   1    62M  0 part 
└─sdb2   8:18   1    62M  0 part 
sr0     11:0    1  60.9M  0 rom  /media/ericm/VBox_GAs_6.1.38
```

in the above you can see the partitions created at /dev/sdb1 and /dev/sdb2.


To create a file system on these partitions you can use one of the "mkfs" commands s+_o:
```
ericm@ericm-VirtualBox:~$ sudo mkfs.vfat /dev/sdb1
[sudo] password for ericm: 
mkfs.fat 4.2 (2021-01-31)
```
Noting the device that is enumerated may differ e.g. /dev/sdc1, /dev/sdd1, ...,  so make certain you get the correct one.  

Now create a mount point,  mount it,  and you can use the mount command to see it mounted. 

```
ericm@ericm-VirtualBox:~$ mkdir mount_point
ericm@ericm-VirtualBox:~$ sudo mount /dev/sdb1 mount_point
ericm@ericm-VirtualBox:~$ mount
sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,relatime)
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
  "  "
/dev/sr0 on /media/ericm/VBox_GAs_6.1.38 type iso9660 (ro,nosuid,nodev,relatime,nojoliet,check=s,map=n,blocksize=2048,uid=1000,gid=1000,dmode=500,fmode=400,iocharset=utf8,uhelper=udisks2)
/var/lib/snapd/snaps/firefox_2391.snap on /snap/firefox/2391 type squashfs (ro,nodev,relatime,errors=continue,x-gdu.hide)
nsfs on /run/snapd/ns/firefox.mnt type nsfs (rw)
/dev/sdb1 on /home/ericm/mount_point type vfat (rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro)
```

Add some files into your newly mounted filesystem.  (use gedit,  echo,  vim, ...)   In one of the files add your name as plain text.

Unmount the file system with "umount".

Copy the raw data from the first partition to the second partition you made,  then mount the second partition.

An old but handy command for copying files is the "dd" command.  So for copying say the blocks of a device to a file with your home directory you could use:
```
root@ericm-VirtualBox:/home/ericm# dd if=/dev/sdb1 of=rawblocks status=progress
64860672 bytes (65 MB, 62 MiB) copied, 178 s, 364 kB/s
126976+0 records in
126976+0 records out
65011712 bytes (65 MB, 62 MiB) copied, 178.169 s, 365 kB/s
```


Do you have the same data within the filesystem in this other partition?

Copy the raw data of the block device into a file within your homedirectory. 

Use grep to see if you can find your name in the raw blocks. e.g.
```
root@ericm-VirtualBox:/home/ericm# grep -a Eric  rawblocks
U�������������Ahello�file.txtHELLOF~1TXT ��XfVfV�XfVEric
```

You can also loopback mount this filesystem. 
```
root@ericm-VirtualBox:/home/ericm# mount -o loop rawblocks mount_point

```

Some filesystems may store different info about file ownership,  also naming can be different between different filesystems.   With vfat (which doesn't have file ownership stored in the file system) if you would like to mount it so files are owned by user 1000 group 1000 you can do:

```
sudo mount -o loop rawblocks mount_point -o rw,uid=1000,gid=1000
```

 What is odd about the below interaction (particularly for someone who just uses Linux)?
```
ericm@ericm-VirtualBox:~/mount_point$ ls
hellofile.txt
ericm@ericm-VirtualBox:~/mount_point$ cat HELLOFILE.TXT
Eric
```


# Activity 2

We will look at a few different filesystems and empirically determine two key limitations of them.  The first is the number of files you can place in a directory.    The second is how big a single file can be.     We do this with two simple python programs.   It would be good to have different people explore different filesystems and we can tabulate and share our results.  

The first python program is lotsofsmall.py:

```
def main():
    for i in range(10000):
        f = open(f'small{i}.txt', "a")
        f.write("Just a small file.")
        f.close()

if __name__ == '__main__':
    main()
```

The second is onebigfile.py:

```
def main():
    f = open(f'big.txt', "a")
    for i in range(10000000):
        f.write("One short line added.\n")
        f.flush()
    f.close()

if __name__ == '__main__':
    main()

```

Also it would be good to create a bigger filesystem (> 4G for looking at file size).   This may take some time so you could move onto the 3rd activity while you wait. 

You may need to adjust the number in the "range" to change the size of the file created. 

To run these programs 'cd' into the mounted filesystem then run:
```
python3 /home/ericm/lotsofsmall.py
```
The details of all the commands you need are from the first activity. 

# Activity 3 - dup dup2 dup3

Read the man page for dup and dup2.   What is the key difference between these two system calls?

Compile and run the twelev_days.c program.   The objective is to save what is printed to standard out in the "twelve_days_of_christmas()" function to the "twelve_days.txt" file,  without modifying the "twelve_days_of_christmas()" function.  This can be done by the use of dup and dup2 system calls only adding code in the main function.   Once the poem is saved you should restore standard out such that the "Bye Bye ..." is printed to the terminal.   The open and close system calls for the file have already been added so they will not need changing. 


