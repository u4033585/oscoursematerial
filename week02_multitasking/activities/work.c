#include <stdio.h>
#include <unistd.h>
#include<stdlib.h>
#include<stdint.h>

// work - a program to give the cpu/memory a bit of a workout
// Eric McCreath 2022
// gcc -o work work.c


void work(long count, long memory_size) {
 
  long i,j;
  uint8_t *data;
  data = malloc(memory_size);
  for(i=0;i<memory_size;i++) data[i] = 0;
  for (i=0;i<count;i++) {
       data[count % memory_size] += 1;
  }
}

int main (int argc, char *argv[]) {
    if (argc != 3) {
       fprintf(stderr,"usage: count memory_size\n");
       exit(1); 
    }
    
    int count_pw = atoi(argv[1]);
    int memory_size_pw = atoi(argv[2]);
    
    work(1L<<count_pw, 1L<<memory_size_pw);
}

