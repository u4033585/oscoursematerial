#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>
#include<stdlib.h>
#include<stdint.h>

//
// badcode
// Eric McCreath 2022
//

// gcc -o badcode badcode.c -lpthread

pthread_t threadA, threadB, threadC;

int data1, data2;

void routineA() {

   for (int i=0;i<100000;i++) {
       data2 += 1;
   }
}

void routineB() {
   data1 = 0;
   for (int i = 0;i<5;i++) {
       data1 = data1 | i<<i;
   }
   data2 = 42;
}

void routineC() {
   for (int i=0;i<100000;i++) {
       data2 -= 1;
   }
}

int main(int argc, char *argv[]) {
    
    pthread_create(&threadA,NULL,(void *) routineA, NULL);
    pthread_create(&threadB,NULL,(void *) routineB, NULL);
    pthread_create(&threadC,NULL,(void *) routineC, NULL);

    pthread_join(threadA,NULL);
    pthread_join(threadB,NULL);
    pthread_join(threadC,NULL);
    printf("Final Results: %d %d\n", data1, data2);
}
