#include <stdio.h>
#include <unistd.h>
#include<stdlib.h>
#include<stdint.h>
#include <sys/types.h>
#include <sys/wait.h>


#define min(A,B) ((A)<(B)?(A):(B))

void work(long count, long memory_size) {
 
  long i,j;
  uint8_t *data;
  data = malloc(memory_size);
  for(i=0;i<memory_size;i++) data[i] = 0;
  for (i=0;i<count;i++) {
       data[count % memory_size] += 1;
  }
}

int main (int argc, char *argv[]) {
    if (argc != 4) {
       fprintf(stderr,"usage: count memory_size at_same_time\n");
       exit(1); 
    }
    
    int count_pw = atoi(argv[1]);
    int memory_size_pw = atoi(argv[2]);
    int at_same_time = atoi(argv[3]);
    
    int still_to_go = 100;
    
    while (still_to_go > 0) {
        int this_time = min(still_to_go, at_same_time);
        for (int i=0; i < this_time;i++) {
           int res = fork();
    	   if (res == 0) {
             work(1L<<count_pw, 1L<<memory_size_pw);
             exit(0);
           }
        }
        for (int i=0; i < this_time;i++) {
           int res;
    	   wait(&res);
        }
        still_to_go -= this_time;
    }
}

