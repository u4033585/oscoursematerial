#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>
#include<stdlib.h>
#include<stdint.h>

//
// Deadlock example 
// Eric McCreath 2022
//

// gcc -o deadlock deadlock.c -lpthread

pthread_t thread[5];
pthread_mutex_t resource[5];

void* routine(void *arg) {
    int thread_idx = (int) (long) (arg);
    int first_res =  thread_idx;
    int second_res = (first_res + 1)%5;
    while (1) {
        printf("%d gaining 1st resource %d\n",thread_idx, first_res);
        pthread_mutex_lock(&(resource[first_res]));
     
        printf("%d gaining 2snd resource %d\n",thread_idx, second_res);
        pthread_mutex_lock(&(resource[second_res]));
      
        printf("%d doing work (and releasing resources %d and %d)\n", thread_idx, first_res, second_res); 
      
        pthread_mutex_unlock(&(resource[first_res]));
        pthread_mutex_unlock(&(resource[second_res]));
     }
}

int main(int argc, char *argv[]) {
    for (int i=0;i<5;i++) {
        pthread_mutex_init(&(resource[i]),NULL);
    }
    for (int i=0;i<5;i++) {
        pthread_create(&(thread[i]),NULL,(void *) routine,(void *) (long) i);
    }
    for (int i=0;i<5;i++) {
        pthread_join((thread[i]),NULL);
    }
}
