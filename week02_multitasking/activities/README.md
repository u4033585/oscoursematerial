# Activity 1 - scheduling

Have a look at the "work.c" program.   The program is run with two integer parameters,  "count" and "memory_size".    How would you expect these to effect the time it takes to run the program.    Without running the program draw a graph of the "execution time" vs "count" for a fixed "memory_size".  Then also the "execution time" vs "memory_size" for a fixed "count".   Don't worry about scale just think what the shape of the graphs should be.   What assumptions have you made in constructing these graphs?

Open another terminal and run "top" in the background as you do the next steps (or the resources monitor GUI).

Compile and run the program with a range of numbers and see what the graphs actually look like.  Push the numbers to the limit.  At what point does memory become problematic?    When does it take more than 20sec to execute?

To run and time the execution you can use the "time" program. e.g.
```
time ./work 10 10 
```

Adjust the parameters such that it take ~2 seconds to run and such that an amount of memory used is close to but under the thrashing limit.   Just imaging you ran "work" with these parameters serially 100 times,  how long would it take to run the program (this is not a trick question)?  We will use these parameters for the following stage.  

Check that your virtual machine is running with more than 1 CPU,   and if not change it so it uses more than 1 CPU.

Read over "work_hundred.c" what is the relationship with "work.c"?   How many times is the "work" part of the code executed?   What does the "at_same_time" parameter do?    What happens in the code if "at_same_time" is set to 1, 2, 4, 10, 25, 50, 100?   Draw a graph of what you think "execution time" vs "at_same_time" would look like.   How does the number CPUs effect this graph?   Run and time the execution of the program.  Do you notice anything interesting?  Or is it just as you expected?

