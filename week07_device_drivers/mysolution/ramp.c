
/* hello world module - Eric McCreath 2005,2006,2008,2010,2012,2016, 2020, 2023 */
/* to compile use:
    make -C  /usr/src/linux-headers-`uname -r` M=$PWD modules
   or if you are inserting the module into a kernel you compiled within 
   your home directory use:
    make -C <the home dir of the kernel> M=$PWD modules
   to install into the kernel use :
    sudo insmod hello.ko
   to test :
    cat /proc/hello
   to remove :
    sudo rmmod hello
*/

#include <linux/fs.h>
#include <linux/init.h>
#include <linux/seq_file.h>
#include <linux/module.h>


char ramp_buf[] = "ABCDEFG";
unsigned int lower = 0;

ssize_t ramp_read (struct file *file, char __user *data, size_t size, loff_t *ppos) {
    *data = (lower ? 0xdf & ramp_buf[(*ppos) % 7] : 0x20 | ramp_buf[(*ppos) % 7]);
    *ppos += 1;
    return 1;

}

ssize_t write_ramp (struct file *file, const char __user *buf, size_t size, loff_t *ppos) {
     ramp_buf[(*ppos) % 7] = *buf;
    *ppos += 1;
    return 1;

}
long ramp_ioctl(struct file *file, unsigned int cmd, unsigned long arg) {
    
    printk("cmd %d %ld\n", cmd, arg);
    lower = cmd;
    return 0;
}

static const struct file_operations ramp_fops = {
	.read = ramp_read,
	.write = write_ramp,
	.unlocked_ioctl = ramp_ioctl,
};


static int __init ramp_init(void)
{
    printk("init ramp\n");
    if (register_chrdev(101, "ramp", &ramp_fops))
		printk("unable to get major\n");

    return 0;
}
static void __exit cleanup_ramp_module(void)
{
    printk("cleanup ramp\n");
    unregister_chrdev(101, "ramp");
    
}


module_init(ramp_init);
module_exit(cleanup_ramp_module);

MODULE_LICENSE("GPL");
