# Activity 1 - Making a device driver


In this activity we are making a device driver that can be loaded via a kernel module. In this case we don't really have a physical device, although, if we did it would be much the same in terms of how code the device driver that executes in kernel mode interacts with user space.   Our device is a character device that just repeatedly output the alphabet, i.e. ABCDE....ABCDE....ABCDE....  Once this is done we will modify it such that it repeatedly echos back whatever is written into it.   Finally, as an optional challenge set up some "ioctl" that enables you configure the device in some way (e.g. so make the ioctl command configure if the text output is converted to upper case or lower case). 

## Step 1 - Some background

Read through the manual pages for "mknod".

You will also need to gain an understanding about the UNIX
device structure.

For a bit of a background read:
* [http://www.linuxjournal.com/article/2597](http://www.linuxjournal.com/article/2597)
* [https://opensourceforu.com/2014/10/an-introduction-to-device-drivers-in-the-linux-kernel/](https://opensourceforu.com/2014/10/an-introduction-to-device-drivers-in-the-linux-kernel/)



## Step 2 - Setting up the module 

Start off by grabbing the hello_module.tar.gz  kernel module from a previous activity. 

Rename the module "ramp", fixing the Makefile and the name of the
main source file.

Strip out all the code that has to do with /proc. We won't be
creating a /proc entry for this kernel module.

Check you can compile your module and insert it into the running kernel.   Noting, if you leave some printk statements 
in the init/clean up functions when you run:
```
$ sudo dmesg
```
you should see them when the module is inserted and removed. 

## Step 3  - Reading from the device 

Declare a file_operations structure and initialise it with a
single entry for a read operation called ramp_read. All other operations can be left unset.
 You may find it useful to look at
"drivers/char/mem.c" in the Linux source code for an example of how to declare a
static file_operations structure (and also register a char device).

Write a ramp_read() function that returns a sequence of characters
from 'A' to 'F'.  Make sure that you advance the *ppos file pointer and
return the number of characters read.   The simplest way of doing this function is one character at a time.  i.e. first time it is called put 'A' in the buffer, increment *ppos, and return 1.  The next time it is called put 'B' in....  A simple way of doing this is make a global string:
```
char ramp_data[] = "ABCEDF";
```
the use the *ppos and modulo function to work out what character to set in the buffer. 

"include/linux/fs.h" has the file_operations structure which gives the exact signature required. 

Add a call to register_chrdev() in your ramp_init() and
unregister_chrdev() in cleanup_ramp_module. Have a look at some code in "drivers/char/" to see examples of using unregister_chrdev.  You will need to find a spare major number,  you can do this using:
```
$ cat /proc/devices
```

Compile your module, insmod it and make sure it appears in the device list. 

## Step 4 - Testing your device from user space

Create a character special file using the command "mknod".  See the
man page for mknod for details.

Test your device by reading from your device file.   A simple way of reading from the device is just using cat:
```
$ cat my_devnod
ABCDEFABCDEF....
```

What happens if you try to write to the device?

## Step 5 - Writing to the device
 
Now you will make the device writeable by supplying a
ramp_write function. The function should accept data and remember it
so that a subsequent read on the device gives the same data back.

The maximum size of the writeable device is up to you, but be warned
that making it unlimited in size will be quite difficult (to start with just make it a fixed sized array within the kernel).

## Step 6 - Adding an ioctl - Optional Challenge

For an optional challenge add an ioctl control on the ramp device so
that your device can be configure to return the data given all in upper case or all in lower case.   This configuration can be set via an ioctl call on the open device.  

To do this you will need to:

* Add a ramp_ioctl function using the unlocked_ioctl field in the file_operations structure. 
* When ramp_ioctl is called you will need to record the setting given (e.g. use 0 for upper case and anything else for lower case).
* Write a test program that configures the device using "open" and "ioctl" (a few lines of c, or maybe python). 






