import socket

class StackCal():
    def __init__(self):
        self.stack = []

    def add_element(self, data):
        line = data.strip('\n')
        try:
            self.stack.append(int(line))
            # print(f'{self.stack}')
            return None
        except ValueError:
            if line == "+":
                v1 = self.stack.pop()
                v2 = self.stack.pop()
                self.stack.append(v1 + v2)
            elif line == "-":
                v1 = self.stack.pop()
                v2 = self.stack.pop()
                self.stack.append(v2 - v1)
            elif line == "*":
                v1 = self.stack.pop()
                v2 = self.stack.pop()
                self.stack.append(v1 * v2)
            elif line == "=":
                v1 = self.stack.pop()
                return v1
            return None


so = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
so.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
so.settimeout(10.0)

cal = StackCal()

so.bind(("0.0.0.0", 4567))
so.listen()
while True:
    try:
        # print('calling accept')
        conn, addr = so.accept()
        # print(f"accepting connection from {addr}")
        done = False
        while not done:
            data = conn.recv(1024)
            #print(f"data:{data}")
            if data == b'':
                done = True
            else:
                res = cal.add_element(data.decode())
                if res is not None:
                    conn.sendall(f'{res}'.encode())
    except socket.timeout:
        print('timeout')

