import socket
import time

commands = ["1", "1", "+", "=", "2", "2", "+", "2", "1", "-", "*", "="]

so = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
so.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
so.settimeout(1.0)
try:
    so.connect(("127.0.0.1", 4567))
    for c in commands:
        so.sendall(f"{c}\n".encode())
        time.sleep(1)
        try:
            data = so.recv(1024)
            print(f"{data}")
        except socket.timeout:
            pass
except ConnectionRefusedError:
    pass
    # print(f'connection refused')

