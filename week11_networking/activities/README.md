

# Activity 1 - Wireshark - reverse engineering a simple TCP protocol

This directory contains 2 python files,  client.py and server.py,  DO NOT LOOK AT THE CODE IN THEM.   The objective of this activity is to reverse engineer, without looking at the code, the TCP protocol between this client and server.   You will use "wireshark" to sniff packets as they pass between the client and server and observer the interaction to determine the protocol.   Noting you many also interact with the server directly using the nc (netcat command).


Open up 2 terminals and run the server in one terminal via:
```
python3 server.py
```

Then in another terminal you can run the client via:
```
python3 client.py 
```

## Part 1 - Determine the protocol and port the server is listening on

A simple way of working out if a network application is listening on a port is to use:
```
sudo netstat -lp
```
If you are doing this from an external computer you could use a port scanning tool like nmap.

What port is the server listening on?

## Part 2 - Sniff the packets and determine the protocol

Install wireshark:
```
sudo apt-get install wireshark-qt
```
(you can just run wireshark using sudo so don't enable the wireshark group)


Run wireshark and record a server/client interaction. 

Look at the data sent between the server and client.   What service is the server providing?  Can you specify the exact semantics the server is using?


## Part 3 - probe the server to confirm your understanding 


Use "nc" to probe the server confirming that it is doing what you expect.

So to run "nc" you just give the ip address (in this case localhost 127.0.0.1) and port and start typing. ie
```
nc 127.0.0.1 <port>
type data that is sent to the server...
```

Confirm your understanding of the protocol by creating a sequence of data to send to the server and predict the exact response,  then feed this sequence to the server and check the expected response. 

Can you crash the server via an interaction?

Once this is done feel free to have a look at the code. 


