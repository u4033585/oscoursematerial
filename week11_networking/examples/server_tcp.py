import socket

# Simple TCP Server Example
# Eric McCreath GPL - 2023

# create a TCP Internet socket
so = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# configure it with a timeout and no delay when setting it up
so.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
so.settimeout(10.0)


# bind with a particular port and set so any IP address can connect to it
so.bind(("0.0.0.0", 4567))

# start listening
so.listen()

while True:
    try:
        print('Accept...')
        conn, addr = so.accept()
        print(f"accepted connection from {addr}")
        done = False
        while not done:
            data = conn.recv(1024).decode()
            print(f"data:{data}")
            if data == "":
                done = True
            else:
                conn.sendall(f'{data.upper()}'.encode())
    except socket.timeout:
        print('Timeout')

