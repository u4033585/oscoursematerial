import socket
import time

# Simple TCP Client Example
# Eric McCreath GPL - 2023

# create a TCP Internet socket
so = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# configure it with a timeout and no delay when setting it up
so.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
so.settimeout(1.0)

try:
    print("Connecting ....")
    so.connect(("127.0.0.1", 4567))
    print("Sending Hello World")
    so.sendall(f"Hello World!".encode())
    time.sleep(1)
    try:
        data = so.recv(1024)
        print(f"{data.decode()}")
    except socket.timeout:
        pass
except ConnectionRefusedError:
    pass


