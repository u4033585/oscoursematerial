# OSCourseMaterial - Operating Systems from a reverse engineering perspective

## Dr Eric McCreath - 2022-2023

## Course Outline

### Week 1 - An Introduction

The first week is a general introduction to the overall topic of Operating Systems.
Content includes:
+ What is an Operating System?
+ Multitasking (Processes and Threads)
+ System and library calls and the relationship between them. (We will explore some key library/system calls within Unix including: open, close, read, write, fork, execv, brk, malloc, free)
+ Using strace and ltrace.

__Activities:__ 

+ Installing Linux within Virtualbox.  
+ Using strace and ltrace to gain an understanding of a processes activity.  

### Weeks 2 and 3 - Multitasking

This week we continue on the topic of multitasking.   This will focus on:
+ multi-threading vs multiprocessors
+ scheduling
+ resource management (deadlocks and race conditions) we will look at synchronisation approaches used to address some of this issues including locks, semaphores, message passing, and briefly consider lock free approaches such as RCU.

__Activities:__ 

+ Reverse engineering programs to understand the synchronisation approaches taken to address deadlocks and race conditions.

### Week 4 - Memory Management part 1

Memory is a key resource provided and managed by the operating system.  As we look at memory management we will focus on:
+ Process loading
+ Memory allocation
+ Partitioning  and segmentation
+ Paging

__Activities:__  

+ To start with we look at creating a dynamic library that is shared between 2 programs. 
+ We will look at the library calls malloc and free and system calls brk and mmap to gain an understanding of how memory is requested and allocated to processes.
+ Exploring the memory mapping of a typical process and how to find, access and read this memory. 

## Week 5 - Memory Management part 2

+ Virtual memory
+ Access Control Mechanisms 

__Activities:__ 

+ This week we have some lab activites that explore compiling the Linux kernel and also compiling and installing kernel modules.
+ There is also an activity related to access control involving permissions, ownership and the suid mechanism. 


### Week 6 - Linking

+ Memory protection (Hardware vs operating system,  address space layout randomisation, Kernel ASLR, Position independent code(PIC/E))
+ object files vs executables
+ static vs dynamic linking
+ early vs late binding
+ relocations
+ PE & ELF

__Activities:__

* Using gdb to see static vs dynamic linking in terms of PIC code with GOTs. 

### Week 7 -  Device Drivers

+ input/output control (ioctl)
+ loadable kernel modules (LKM)
+ Rootkits

__Activities:__  

+ Writing and installing a device driver via a kernel module. 

### Week 8 - File Systems

+ block vs character devices
+ organisation (files, folders, links (hard and soft))
+ implementation abstractions

__Activities:__ 

+ making and mounting a file system
+ limitations of filesystems
+ dup/dup2 system calls


### Week 9 - File Systems

+ kernel integration

__Activities:__ 


+ Reverse engineering a teaching via mounting modifying and looking at raw blocks. 
+ Filesystem code and fixing some bugs in a filesystem implementation. 

### Week 10 - File Systems

+ soft deletion
+ boot records
+ media constraints

__Activities:__

+ Soft deletion and looking how what happens when you "delete" a file in different filesystems.
+ Tools for securely deleting files. 

### Week 11 - Networking

+ dispatch and delivery
+ process mapping
+ kernel integration

__Activities:__ 

+ Wireshark and reverse engineering a simple TCP protocol

### Week 12 - Multi-tenancy

+ user mode vs supervisor mode
+ access controls, to and between (processes, memory, storage, other resources)
+ auditing 
+ scheduling (cgroups)
+ shells

__Activities:__  

+ Setting up cgroups to limit cpu usage
+ Setting up namespaces
+ Installing and using an audit deamon


## Text Book

+ Arpaci-Dusseau, Operating Systems: Three Easy Pieces  [http://pages.cs.wisc.edu/~remzi/OSTEP/](http://pages.cs.wisc.edu/~remzi/OSTEP/)
Free online version available. 

# Other useful books

+ _Stallings, William Operating Systems,_ Prentice-Hall, seventh edition, 2012 (Gives a good general background.)
+ _Operating System Concepts 5th edition by Silberschatz(and others)_ Addison Wesley (Clearly explains may os concepts.) 
+ _Modern Operating Systems by Tanenbaum_ Prentice-Hall.   (Covers similar material to that of Silberschatz al et  although it goes into more technical details.)
+ _Operating Systems - Design and Implementation by Tanenbaum and Woodhull_ Prentice-Hall. (Covers the fundamental principles of operating system and discusses one particular operating system, namely minix, looking at how the theory may be put into practice. Includes the Minix source code.)
+ _Operating Systems - A Modern Perspective second edition by Gary Nutt._ (Provides good examples of concepts within OS.) 

















