 
# Activity 1 - Compiling and Running the Linux Kernel
 
## Getting the linux source code
 
 It is good to check which version of the kernel you are running.  This can be done with:
```
 uname -a
```
 Then to obtain the linux source code:
```
 sudo apt-get update
 sudo apt-get install linux-source
```
 This places it in "/usr/src" in a tar.bz2 file.
 
 The idea is to uncompress it into your home directory and then compile the kernel up as a normal user.  So from your home directory run:
```
 tar xfj /usr/src/linux-source-5.15.0.tar.bz2
```
 
This should place the source code in your home directory in "linux-source-5.15.0".  cd into this directory and have a look around the source code. 
 
 
Track down the code that relates to:
+ device drivers,
+ networking,
+ file systems, and
+ the particular CPU the kernel works on.

Which one do you think would involve the most code? Why?

One simple way to measure this is to find out the size of files within the different directories.  This can be done with:
```
du -sh *
```
What does this tell you?

Try to find the point in the source code that writes out the contents of
/proc/cpuinfo when you ask for it.

What system call is used when you delete a file ("rm" command)?  Can you think why it is called this?  Track down the code in the kernel for that system call.  (hint - strace is your friend and system call entry points use a macro for the signature)   

 
# Obtaining the required tools to compile the kernel
 
 There are a number of tools you need to compile the kernel, these can be obtained with:
```
 sudo apt-get install build-essential libncurses-dev bison flex libssl-dev libelf-dev pkg-config dwarves
```
 
# Configuring the kernel
 
Before you compile the kernel you need to make a ".config" file in the root directory of the kernel source.  This could be done with:
```
 make menuconfig
```
 run from the source directory.  Run menuconfig and explore some of the possible options.  However this is rather involved and you are unlike to get a configuration that works with Ubuntu (so we will not use this to create our ".config" file,  rather,  we will copy the one from the running kernel which we know works).   This can be done with:
```
cp /boot/config-$(uname -r) .config
```
The down side of this is the config generated is big and has lots of modules,  so it takes a long long time to compile everything!

To get it working on the current version I had to do a few tweeks:
+ disable CONFIG_X86_X32
+ remove the "CONFIG_SYSTEM_TRUSTED_KEYS" setting making it empty. ie:
```
CONFIG_SYSTEM_TRUSTED_KEYS=""
```
+ also remove CONFIG_SYSTEM_REVOCATION_KEYS making it empty:
```
CONFIG_SYSTEM_REVOCATION_KEYS=""
```
+ disable:

```
CONFIG_DEBUG_INFO_BTF=n
```

## To compile the kernel and modules

From the linux source directory run:
```
make -j4
```

Once compiled you can install the modules and then the kernel with:
```
sudo make modules_install
sudo make install
```


So we should now have the kernel in /boot and the modules in /lib/modules.   Have a look and see if they have been added. 


## Setting Up the bootloader

"grub" is the program that loads the kerenl.  We need to 
modify the "grub" setup such
 that such when the machine is rebooted it goes to a menu option rather than always running the same kernel.   This can be done by changing the file:
```
 /etc/default/grub
```
Such that GRUB_TIMEOUT is set to -1. 


We can now install and update grub:
```
sudo grub-install /dev/sda
sudo update-grub
```

Reboot.  And select your new kernel in the grub menu.  To see which kernel you are running you can type:
```
uname -r
```

Now you are running on your newly made kernel!!!

Modify the kernel sources to add your name to the contents of /proc/cpuinfo. Then build the kernel using the command "make", along with the "sudo make install" commands. The rest of the commands should not need redoing.

Did your modification work? 

## Step 4 - dont_delete 

Modify the kernel source code so that it is impossible to delete a
file called "dont_delete". Compile your kernel (it should be fast
this time as most of it hasn't changed). See if your change
worked. 

You only need to add one line of code to the source code. However, working out at what point to place it is the trick!  Hint - There is code in the kernel that determine if you "may delete" a file or not.


# A "hello world" kernel module 

Download the kernel module and compile it then install it in your kernel using the
<i>insmod</i> command. Also try <i>lsmod</i> and <i>rmmod</i>. (you may need the -f option.)

You can obtain a copy of the kernel module using from:

+ [hello_module.tgz](../hello_module.tgz)  

You can use:
```
tar xfz hello_module.tgz
```
to extract the file.  Within the hello.c file there is instruction on how to compile and run it. 

Have a look at the source code to the module and try to work out what
the module does.  How do you get it to print "hello world"?

Modify what is printed.   Compile and reload this new module.



#Activity 2 - Access Control

Create 2 other users:
```
sudo adduser jill
sudo adduser bob
```

Create a few files and have a play with changing ownership and permissions on these files (chown and chmod commands).   Reflect on the limitations/problems with this approach for access control?   What commands could you do as a regular used?  Which ones did you need to be root?

Are the ownership/permission assoicated with the "file"(ie the data that can linked to from many filenames)  or the "filename" (ie the directory entry)??   To check this out you can make a file and a hard link to it (so 1 file with two names):
```
echo hello > hello.txt
ln hello.txt hello2.txt
```
Then modify permissions in one file and see if they also change in the other.


With these users you have 2 tasks.    Noting at the bottom there is a few hints relating to the commands that you will need. 

## Task 1 - sgid

Add jill to your group.   Now create a directory that is readable and writeable by people in your group but not others (check this is the case with both bob and jill).   Also set this directory up such that when others add files to this directory they have the same group as the directory.  Once again check this works okay.    


## Task 2 - suid


Now write a program that any other users can execute.  However, when executed the permissions of the program are change such that it is now only readable and writeable by the owner,  and is also nolonger executable by others.  To get you started copy the program from the examples directory. 

So an interaction should look like:
```
jill@gtxg5:/home/ericm/oscoursematerial/week05_virtualmemory/examples/suid$ ls -l disable_execution
-rwsrwsr-x 1 ericm ericm 16928 Feb 15 23:21 disable_execution
jill@gtxg5:/home/ericm/oscoursematerial/week05_virtualmemory/examples/suid$ ./disable_execution 
Real UID	= 1002
Effective UID	= 1000
Real GID	= 1003
Effective GID	= 1000
jill@gtxg5:/home/ericm/oscoursematerial/week05_virtualmemory/examples/suid$ ls -l disable_execution
-rw------- 1 ericm ericm 16928 Feb 15 23:21 disable_execution
jill@gtxg5:/home/ericm/oscoursematerial/week05_virtualmemory/examples/suid$ ./disable_execution 
bash: ./disable_execution: Permission denied
```
 
Notice in the above the user executing the commands is 'jill',  whereas, the files are owned by 'ericm'. 

Hints - 
+ "sudo usermod -a -G a_group user_to_add" - add a user to a group
+ "sudo su jill" - changes the user so you are now "jill" within that terminal.  
+ "chown user.group file" changes a files user and group ownership
+ "chmod 660 file" changes the files permission (numbers octal). 
+ "chmod g+s directory" sets the SetGID bit for that directory.
+ "chmod u+s file" sets the SetUID bit for that file. 
+ "man 2 chmod" gives you the include files and signiture for chmod that you call from within c.
+ "gcc -o myprogram myprogram.c" compiles up a c program.
+ "./myprogram" runs it. 
 







 
 
