# SUID

In Unix access control is done via file ownership and permissions.   However this is rather course.  SUID (Set user id) provides a more flexible mechanism to enforce a desired policy. 

So in this example we wish to let other users just read the last line of a log file, but not be able to read the rest of the file.

We make a program that just reads the last line of a logfile, called lastline.   The we set it up so the process changes is id while it is being executed.   This can be done with:  
```
chmod ug+s lastline
```
