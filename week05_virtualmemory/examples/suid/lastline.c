#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
 
#define MAXLINE 100



int main(int argc, char *argv[])
{
    printf("Real UID\t= %d\n", getuid());
    printf("Effective UID\t= %d\n", geteuid());
    printf("Real GID\t= %d\n", getgid());
    printf("Effective GID\t= %d\n", getegid());

    FILE *logfile;
   
    char buff[MAXLINE+1];

    if ((logfile =  fopen("logfile","r")) != NULL) {
       while(!feof(logfile)) {
          fscanf(logfile, "%[^\n]\n", buff);
       }
       printf("%s\n",buff);
       fclose(logfile);
       return EXIT_SUCCESS;
    } else {
       printf("problem reading file\n");
       return -1;
    }
}

