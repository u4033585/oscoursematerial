# Activity 1 -  Setting up cgroups to limit cpu usages

cgroups can be configured directly via "files" in /sys/fs/cgroup, there is also tools such as "cgroup-tools" which enables you to set up and configure cgroups.  However, as the direct way is simple and as it also gives us an idea of how the kernel is configured, we will use this approach.   Noting that there is a "v1" and "v2" way of doing this,  they are different,  as our virtual machine uses "v2" we will use this approach.

For a process to execute that will get the cores of the computer working hard we will use "stress".  To install:

```
sudo apt-get install stresss
```

Open an extra terminal with top running in it for the rest of the activity.  Set up your windows such that this terminal is off on the side but still viewable. 


In one terminal run stress with 16 processes:
```
stress -c 16
```
You should see the 16 processes running all with about the same CPU usage.   We aim to get one of the processes to use less (or more) of the CPU than the others.  We basically create a control group for this process,  move it into the control group then vary its configuration.


Set up a terminal which can execute commands as root:
```
sudo su
cd /sys/fs/cgroup/
```

Make a sub control group for the process:
```
mkdir myprocesscontrol
```

Have a look at what the root can control:
```
cat cgroup.controllers
```
and what sub groups can control:
```
cat cgroup.subtree_control
```
now we want our subgroup to be able to control the cpu so we need to add this to the "cgroup.subtree_control" via:
```
echo "+cpu" > cgroup.subtree_control
```
you can check it has been added via cating the file.  You can also look at the "cgroup.controllers" within the "myprocesscontrol" directory. 

If you have a look at the process in the root control group (cat cgroup.procs),  you should see the pids of the stress processes.
To move one of them into the new control group you (say the pid of one of the stress processes is 54321):
```
cd myprocesscontrol 
echo 54321 > cgroup.procs
```

You can now vary the weight of the processes within "myprocesscontrol".  Try a few different values such as:
```
echo 2 > cpu.weight
```
or
```
echo 2000 > cpu.weight
```
How does the CPU usage for this process change? 

Have a look at: https://docs.kernel.org/admin-guide/cgroup-v2.html
 

# Activity 2 - Setting up a new namespace 

The objective of this activity is create a container that is executing a bash script that has an extra filesystem mounted.  So this process, using the namespace mechanisim,  has a different veiw of what is currently mounted. 

The below commands will create a loopback image that you can use to mount when needed.
```
dd if=/dev/zero of=test.img count=10000
mkfs.vfat test.img
```
Then when you need to mount it you can use:
```
mount -o loop test.img vfat
```

Have a look at the man page for "unshare".   Work out the parmeter you need to add to the unshare command such that you can create a new namespace that will let you add a new mount point visable in the new namespace but not in the original one. 


Once done from a normal bash shell you would not see the mounted file system:
```
ericm@gpgpu:~/tmp/test$ mount | grep test.imp
ericm@gpgpu:~/tmp/test$ 
```
However at the same time from the bash script in the container you would be able to see and access the mounted filesystem:
```
ericm@gpgpu:~/tmp/test$ mount | grep test.imp
/home/ericm/tmp/test/test.imp on /home/ericm/tmp/test/vfat type vfat (rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro)
ericm@gpgpu:~/tmp/test$
```

Hints - the "su" command lets you change to any user when executed from root. 

# Activity 3 - Installing the audit deamon (if we have time)

Install the audit deamon:
```
sudo apt-get install auditd
```

And an audit watch on the /etc/passwd file via:
```
sudo auditctl -w /etc/passwd -p wa -k user-modify
``` 

Now change this file via adding a new user:
```
sudo adduser testuser1
```

Have a look in the log file /var/log/audit/audit.log and see what information is recorded. 






