# Activity 1 - Race conditions 

The "badcode.c"  program creates 3 threads,  one of them does some initialization the other 2 threads work on shared data using data initializated from the first.  The original implementer didn't worry about syncronization as it was run on a single threaded system so it worked okay.   However,  when others ran it the lack of syncronization exposed race problems.    Read over the code and work out the intention of each of the threads.  Determine what is the shared data and what are the potential syncronization problems.   Compile and run the program,  are these problem exposed when you execute the code?

Copy this code into a new program called "goodcode.c".  Fix the problem using a semephore and a lock.

How does the addition of sychronization effect how long it takes to execute the program. 

Hint - Some randomly selected lines of code that help:
```
sem_t sync;
pthread_mutex_t mutex;
pthread_mutex_lock(&mutex);
pthread_mutex_unlock(&mutex);
sem_wait(&sync);
sem_post(&sync);
```

# Activity 2 - Deadlocks

The "deadlock.c" program creates 5 threads.   Each thread has some work to do (in the code it is just printing out "doing work").   However for a thread to do it's work it must first obtain exclusive access to 2 of the 5 resources (they are just imaginary resources in the code).   The resources are guarded by 5 locks,  so a thread will gain 2 locks do its work then release the locks.   They could potentially run without deadlocking.  There could even be 2 threads doing work at the same time.   Read over the code, draw a resource dependency graph and analysis it to see if there are any potential problems.   What are they? And in what situations do they occur?

Compile and run the code.   What happens?  Does it always do the same thing?
 
Think of a few ways you could modify the code to fixing this deadlock problem.  Rank these approaches in terms of complexity and they how they constrain the execution of the program.

Select one of these approaches and modify the code fixing the deadlock problem.  Test your solution.

This is probably one of the most well known deadlock scenarios, does anyone know what it is called?
