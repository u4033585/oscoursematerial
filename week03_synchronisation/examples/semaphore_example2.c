#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>


// semaphore_example2.c - some example code using semaphores to order execution of code sections
// Eric McCreath 2022
// gcc -o semaphore_example2 semaphore_example2.c  -lpthread

pthread_t threadA, threadB;
sem_t syncBA;


int data = 0;

void routineA() {
    //sem_wait(&syncBA);
    printf("A's code  \n");  
}

void routineB() {
    printf("B's code \n");  
   // sem_post(&syncBA);
}



int main() {
    sem_init(&syncBA,0,0);
    
    pthread_create(&threadA,NULL,(void *) routineA,NULL);
    pthread_create(&threadB,NULL,(void *) routineB,NULL);
    
    pthread_join(threadA, NULL);
    pthread_join(threadB, NULL);
    
    return 0;
}
