#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>


// semaphore_example1.c - some example code using semaphores as a lock
// Eric McCreath 2022
// gcc -o semaphore_example1 semaphore_example1.c  -lpthread

pthread_t threadA, threadB;
sem_t mutexAB;


int data = 0;

void routineA() {
    int j;

    printf("A starting\n");   
 
    sem_wait(&mutexAB);
  
    printf("A's code that accesss shared data \n");  
    data++;
   
    sem_post(&mutexAB);
    printf("A ending\n");   
}

void routineB() {
    int j;

    printf("B starting\n");   
 
    sem_wait(&mutexAB);
  
    printf("B's code that accesss shared data \n");  
    data++;
   
    sem_post(&mutexAB);
    printf("B ending\n");   
}



int main() {
    sem_init(&mutexAB,0,1);
    
    pthread_create(&threadA,NULL,(void *) routineA,NULL);
    pthread_create(&threadB,NULL,(void *) routineB,NULL);
    
    pthread_join(threadA, NULL);
    pthread_join(threadB, NULL);
    printf("Shared data at end: %d\n", data);
    
    return 0;
}
