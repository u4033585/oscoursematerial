

# Activity 1 - Soft Deletion Activity

The aim of this activity is to explore soft deletion on different filesystems.   The question we wish to answer is "What information about the file is left over after a file is deleted?"   This will be for a number of different filesystems (including bfs, ext2, msdos, minix, ntfs).


Rather than me giving you a list of commands to execute to help answer this question what I would like you to do is collectively come up with a plan for answering the question.   Then execute your plan and summarise your answer.

Key decisions for the plan include:
 + exactly what info you would like to include  
 + how to conduct the experiment
 + filesystem allocation for different people

We can tabulate the results on the whiteboard.

To help I have included a short python program that generates an 8K binary file.  The file contains a repeated signature interleaved with a 4 byte big endian counting integer.    The signature is the same byte repeated 4 times and the byte value is given as a parameter to the python program.  Thus when run with 42 as the parameter we would get:

```
ericm@gpgpu:~/PycharmProjects/hughplot$ python3 counting_file.py 42
ericm@gpgpu:~/PycharmProjects/hughplot$ od -x --endian=big counting_file_2a.bin  
0000000 2a2a 2a2a 0000 0000 2a2a 2a2a 0000 0001
0000020 2a2a 2a2a 0000 0002 2a2a 2a2a 0000 0003
0000040 2a2a 2a2a 0000 0004 2a2a 2a2a 0000 0005
0000060 2a2a 2a2a 0000 0006 2a2a 2a2a 0000 0007
0000100 2a2a 2a2a 0000 0008 2a2a 2a2a 0000 0009
0000120 2a2a 2a2a 0000 000a 2a2a 2a2a 0000 000b
0000140 2a2a 2a2a 0000 000c 2a2a 2a2a 0000 000d
0000160 2a2a 2a2a 0000 000e 2a2a 2a2a 0000 000f
0000200 2a2a 2a2a 0000 0010 2a2a 2a2a 0000 0011
0000220 2a2a 2a2a 0000 0012 2a2a 2a2a 0000 0013
0000240 2a2a 2a2a 0000 0014 2a2a 2a2a 0000 0015
0000260 2a2a 2a2a 0000 0016 2a2a 2a2a 0000 0017
0000300 2a2a 2a2a 0000 0018 2a2a 2a2a 0000 0019
0000320 2a2a 2a2a 0000 001a 2a2a 2a2a 0000 001b
0000340 2a2a 2a2a 0000 001c 2a2a 2a2a 0000 001d
.....

```

So if you create some files using this program then delete these files you can look at the raw blocks of the device and see what parts of which files remain after being deleted. 

Also if you loopback mount the filesystem you can create snapshots the filesystem by copying blocks to a different file and then see how these snapshots change.  (try something like: "diff <(hexdump fsblocks_before) <(hexdump fsblocks_after) ")

# Activity 2 - Tools for securely deleteing files

Have a look at: https://www.geeksforgeeks.org/tools-to-securely-delete-files-from-linux/

Download the "secure-delete" package and test the effectiveness of "srm" along with "sfill" on your filesystem from activity 1.




