import sys
def main(sig):
    f = open(f'counting_file_{sig:02x}.bin', "ab")
    for i in range(1024):
        for j in range(4):
            f.write(sig.to_bytes(1,byteorder="big"))
        f.write(i.to_bytes(4,byteorder="big"))
    f.close()

if __name__ == '__main__':
    if len(sys.argv) == 2:
        try:
            v = int(sys.argv[1])
            main(v)
        except:
            print("expected int")
    else:
        print("expected int argument")
